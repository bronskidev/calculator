package ru.gb.aoj.calculator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity implements Serializable {

    private static final String KEY_MAIN_SCREEN = "mainScreen";
    private static final String KEY_MEMORY_SCREEN = "memoryScreen";

    Button button0, button1, button2, button3, button4, button5, button6, button7, button8, button9,
            buttonPlus, buttonMinus, buttonEqual, buttonDivide, buttonMultiply, buttonPercent, buttonClearAll, buttonClearOne, buttonDot, buttonSettings;
    TextView memoryScreen, mainScreen;
    String process;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button0 = findViewById(R.id.button_0);
        button1 = findViewById(R.id.button_1);
        button2 = findViewById(R.id.button_2);
        button3 = findViewById(R.id.button_3);
        button4 = findViewById(R.id.button_4);
        button5 = findViewById(R.id.button_5);
        button6 = findViewById(R.id.button_6);
        button7 = findViewById(R.id.button_7);
        button8 = findViewById(R.id.button_8);
        button9 = findViewById(R.id.button_9);

        buttonPlus = findViewById(R.id.button_plus);
        buttonMinus = findViewById(R.id.button_minus);
        buttonDivide = findViewById(R.id.button_divide);
        buttonMultiply = findViewById(R.id.button_multiply);

        buttonEqual = findViewById(R.id.button_equal);

        buttonClearAll = findViewById(R.id.button_clear_all);
        buttonClearOne = findViewById(R.id.button_clear_one);
        buttonDot = findViewById(R.id.button_dot);
        buttonPercent = findViewById(R.id.button_percent);
        buttonSettings = findViewById(R.id.settings);

        memoryScreen = findViewById(R.id.memory_screen);
        mainScreen = findViewById(R.id.main_screen);

        buttonClearAll.setOnClickListener(v -> {
            mainScreen.setText("");
            memoryScreen.setText("");
        });

        button0.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "0");
        });

        button1.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "1");
        });

        button2.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "2");
        });

        button3.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "3");
        });

        button4.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "4");
        });

        button5.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "5");
        });

        button6.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "6");
        });

        button7.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "7");
        });

        button8.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "8");
        });

        button9.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "9");
        });

        buttonPlus.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "+");
        });

        buttonMinus.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "-");
        });

        buttonDivide.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "÷");
        });

        buttonMultiply.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "×");
        });

        buttonDot.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + ".");
        });

        buttonPercent.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            memoryScreen.setText(process + "%");
        });

        buttonClearOne.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();
            if (process.length() > 0){
                memoryScreen.setText(process.substring(0, (process.length() - 1)));
            }
        });

        buttonEqual.setOnClickListener(v -> {
            process = memoryScreen.getText().toString();

            process = process.replaceAll("×","*");
            process = process.replaceAll("%","/100");
            process = process.replaceAll("÷","/");

            Context rhino = Context.enter();

            rhino.setOptimizationLevel(-1);

            String finalResult = "";

            try {
                Scriptable scriptable = rhino.initStandardObjects();
                finalResult = rhino.evaluateString(scriptable, process,"javascript",1,null).toString();
            }catch (Exception e){
                finalResult="0";
            }

            mainScreen.setText(finalResult);
        });

        buttonSettings.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivityForResult(intent, RESULT_OK);
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_MAIN_SCREEN, mainScreen.getText().toString());
        outState.putString(KEY_MEMORY_SCREEN, memoryScreen.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mainScreen.setText(savedInstanceState.getString(KEY_MAIN_SCREEN));
        memoryScreen.setText(savedInstanceState.getString(KEY_MEMORY_SCREEN));
    }

}